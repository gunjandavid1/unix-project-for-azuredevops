# syntax=docker/dockerfile:1
FROM ubuntu:18.04
COPY . /app
RUN chmod 755 /app/*.sh
CMD sh /app/*.sh
